import logging
from telegram import Update
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, filters
from telegram.ext import MessageHandler
import os
from dotenv import load_dotenv
load_dotenv()
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)
TOKEN = os.getenv('TOKEN')
print("token is",TOKEN)

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a private bot to send files to another group!")


async def echo(update: Update, context: ContextTypes.DEFAULT_TYPE):
    CHAT_ID = os.getenv('CHAT_ID')
    message = update.message
    if (CHAT_ID != update.effective_chat.id):
       await context.bot.copy_message(chat_id=CHAT_ID, from_chat_id=update.effective_chat.id, message_id=message.message_id)

if __name__ == '__main__':
    
    application = ApplicationBuilder().token(TOKEN).build()
    
    start_handler = CommandHandler('start', start)
    echo_handler = MessageHandler(filters.Document.ALL, echo)
    application.add_handler(start_handler)
    application.add_handler(echo_handler)
    application.run_polling()

